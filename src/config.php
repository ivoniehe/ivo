<?php
spl_autoload_register(function ($class) {
    include 'classes/' . $class . '.php';
});

include_once __DIR__ . '/functions/entities.php';

$db = new Database();
$sec = new Security();
$user = new User();