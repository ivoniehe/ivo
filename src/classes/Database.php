<?php

class Database {
    private $db;

    public function __construct() {
        $this->db = new PDO('mysql:host=localhost;dbname=ivo_db', 'root', 'root');
    }

    public function insert($table, array $data) {
        if(empty($data)) {
            throw new InvalidArgumentException('Array can not be empty.');
        }
        if(!is_string($table)) {
            throw new InvalidArgumentException('Table must be a string.');
        }

        $fields = '`' . implode('`, `', array_keys($data)) . '`';
        $placeHolders = ':' . implode(', :', array_keys($data));

        $sql = "INSERT INTO {$table} ($fields) VALUES ({$placeHolders})";

        $stmt = $this->db->prepare($sql);

        foreach($data as $placeHolder => &$value) {
            $placeHolder = ':' . $placeHolder;
            $stmt->bindParam($placeHolder, $value);
        }

        if(!$stmt->execute()) {
            var_dump($stmt);
            throw new ErrorException('Could not execute query');
        }

        if($stmt->rowCount() === 0) {
            throw new ErrorException('Could not insert data into table.');
        }
        return true;
    }

    public function update($table, $id, array $data) {
        $query = "UPDATE {$table} SET";
        $values = array();

        foreach($data as $placeHolder => $value) {
            $query .= ' '.$placeHolder.' = :'.$placeHolder.',';
            $values[':'.$placeHolder] = $value;
        }

        $query = substr($query, 0, -1).' WHERE id = '.$id;

        $stmt = $this->db->prepare($query);

        return $stmt->execute($values);
    }

    public function updateUID($table, $id, array $data) {
        $query = "UPDATE {$table} SET";
        $values = array();

        foreach($data as $placeHolder => $value) {
            $query .= ' '.$placeHolder.' = :'.$placeHolder.',';
            $values[':'.$placeHolder] = $value;
        }

        $query = substr($query, 0, -1).'; WHERE uniqid = '.$id;

        $stmt = $this->db->prepare($query);

        return $stmt->execute($values);
    }

    public function get($table, $id) {
        $stmt = $this->db->prepare("SELECT * FROM {$table} WHERE id = {$id}");
        $stmt->execute();
        return $stmt->fetch();
    }

    public function getIssue($uniqID) {
        $stmt = $this->db->prepare("SELECT * FROM issues WHERE uniqid = :uniqid");
        $stmt->bindParam(':uniqid', $uniqID);
        $stmt->execute();
        return $stmt->fetch();
    }

    public function getUser($uniqID) {
        $stmt = $this->db->prepare("SELECT * FROM users WHERE uniqid = :uniqid");
        $stmt->bindParam(':uniqid', $uniqID);
        $stmt->execute();
        return $stmt->fetch();
    }

    public function getAll($table) {
        $stmt = $this->db->prepare("SELECT * FROM {$table}");
        $stmt->execute();
        return $stmt->fetchAll();
    }

    public function delete($table, $id) {
        return $this->db->prepare('DELETE FROM '. $table .' WHERE id=' . $id)->execute();
    }

    public function deleteUser($uniqID) {
        $stmt = $this->db->prepare("DELETE FROM users WHERE uniqid = :uniqid");
        $stmt->bindParam(':uniqid', $uniqID);
        return $stmt->execute();
    }

    public function deleteIssue($uniqID) {
        $stmt = $this->db->prepare("DELETE FROM issues WHERE uniqid = :uniqid");
        $stmt->bindParam(':uniqid', $uniqID);
        return $stmt->execute();
    }
}