<?php

class Security {
    public function hashPass($password) {
        $options = [
            'cost' => 11,
        ];

        return password_hash($password, PASSWORD_BCRYPT, $options);
    }

    public function verifyPassword($password, $data) {
        if(password_verify($password, $data)) {
            return true;
        } else {
            return false;
        }
    }

}