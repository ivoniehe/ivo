-- phpMyAdmin SQL Dump
-- version 4.2.10
-- http://www.phpmyadmin.net
--
-- Machine: localhost:8889
-- Gegenereerd op: 23 feb 2017 om 14:44
-- Serverversie: 5.5.38
-- PHP-versie: 5.6.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Databank: `ivo_db`
--

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `issues`
--

CREATE TABLE `issues` (
  `id` int(1) NOT NULL,
  `uniqid` varchar(20) NOT NULL,
  `header` varchar(32) NOT NULL,
  `detail` text NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `reprod` varchar(5) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `uniqid` varchar(20) NOT NULL,
  `username` varchar(32) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(60) NOT NULL,
  `firstname` varchar(32) DEFAULT NULL,
  `lastname` varchar(64) DEFAULT NULL,
  `img` varchar(32) DEFAULT NULL,
  `country` varchar(127) DEFAULT NULL,
  `city` varchar(32) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `users`
--

INSERT INTO `users` (`id`, `uniqid`, `username`, `email`, `password`, `firstname`, `lastname`, `img`, `country`, `city`) VALUES
  (1, 'spp4ks', 'Max', 'max.vanstijn@yahoo.com', '$2y$11$./0gyKEu9kbCRSxg5HiuiuxPzyhooBRsmO/3UVw7i5ZUMZKDJafRO', 'Max', 'van Stijn', 'max.png', 'The Netherlands', 'Weert'),
  (2, '84f9yn', 'Stan', 'stan-smulders@live.nl', '$2y$11$A2d8981MXOMNLbrMo4Gd2e03ucLLs25JBYv.ftCELWiVIifn7dny6', 'Stan', 'Smulders', 'stan.png', 'The Netherlands', 'Weert');

--
-- Indexen voor geëxporteerde tabellen
--

--
-- Indexen voor tabel `issues`
--
ALTER TABLE `issues`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `username` (`username`,`email`);

--
-- AUTO_INCREMENT voor geëxporteerde tabellen
--

--
-- AUTO_INCREMENT voor een tabel `issues`
--
ALTER TABLE `issues`
  MODIFY `id` int(1) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT voor een tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;