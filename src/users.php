<?php require __DIR__ . '/config.php'; ?>
<table>
    <thead>
    <tr>
        <th>ID</th>
        <th>Firstname</th>
        <th>Lastname</th>
        <th>Username</th>
        <th>Email</th>
        <th>Country</th>
        <th>City</th>
        <th>Actions</th>
    </tr>
    </thead>
    <tbody>
        <?php
        $users = $db->getAll('users');
        foreach ($users as $user) { ?>
            <tr>
                <td>#<a href="user/user.php?user=<?= $user['uniqid'] ?>"><?= $user['id'] ?></a></td>
                <td><?= $user['firstname'] ?></td>
                <td><?= $user['lastname'] ?></td>
                <td><?= $user['username'] ?></td>
                <td><?= $user['email'] ?></td>
                <td><?= $user['country'] ?></td>
                <td><?= $user['city'] ?></td>
                <td>
                    <a href="user/edit.php?user=<?= $user['uniqid'] ?>">Edit</a></i>
                    <a href="user/delete.php?user=<?= $user['uniqid'] ?>">Delete</a>
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>