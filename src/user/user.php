<?php require __DIR__ . '/../config.php';

if(!isset($_GET['user'])) {
    throw new DomainException('ERR: NO USER SET.');
} else {
    if(!$db->getUser($_GET['user'])) {
        throw new DomainException('ERR: NO USER WITH ID.');
    } else {
        $user = $db->getUser($_GET['user']);
    }
} ?>
Viewing User: <?= $user['username'] ?>
<br />
Email: <?= $user['email'] ?>
<br />
Country: <?= $user['country'] ?>
<br />
City: <?= $user['city'] ?>