<?php require __DIR__ . '/../config.php';

if(!isset($_POST['submit'])) { ?>
    <h1>Register</h1>
    <form action="register.php" method="post">
        <label>Username:
            <input type="text" placeholder="( ͡° ͜ʖ ͡°)" name="username">
        </label>
        <label>Email:
            <input type="text" placeholder="( ͡° ͜ʖ ͡°)@( ͡° ͜ʖ ͡°).( ͡° ͜ʖ ͡°)" name="email">
        </label>
        <label>Password:
            <input type="password" placeholder="( ͡° ͜ʖ ͡°)" name="password">
        </label>
        <label>Firstname:
            <input type="text" placeholder="Lenny( ͡° ͜ʖ ͡°)" name="firstname">
        </label>
        <label>Lastname:
            <input type="text" placeholder="( ͡° ͜ʖ ͡°)Lenny" name="lastname">
        </label>
        <label>Country:
            <input type="text" placeholder="( ͡° ͜ʖ ͡°)land" name="country">
        </label>
        <label>City:
            <input type="text" placeholder="New ( ͡° ͜ʖ ͡°)" name="city">
        </label>
        <button type="submit" name="submit">Create</button>
    </form>
    </body>
    </html>
<?php } else {
    $uniqID = base_convert(sprintf('%u', crc32(serialize($_POST['username']))), 10, 36);
    $data = [
        'uniqid' => $uniqID,
        'username' => $_POST['username'],
        'email' => $_POST['email'],
        'password' => $sec->hashPass($_POST['password']),
        'firstname' => $_POST['firstname'],
        'lastname' => $_POST['lastname'],
        'country' => $_POST['country'],
        'city' => $_POST['city'],
    ];

    if(!$db->insert('users', $data)) { ?>

    <h1>Issue while registering.</h1>
    <form action="register.php" method="post">
        <label>Username:
            <input type="text" placeholder="( ͡° ͜ʖ ͡°)" name="username">
        </label>
        <label>Email:
            <input type="text" placeholder="( ͡° ͜ʖ ͡°)@( ͡° ͜ʖ ͡°)" name="email">
        </label>
        <label>Password:
            <input type="password" placeholder="( ͡° ͜ʖ ͡°)" name="password">
        </label>
        <label>Firstname:
            <input type="text" placeholder="( ͡° ͜ʖ ͡°)" name="firstname">
        </label>
        <label>Lastname:
            <input type="text" placeholder="( ͡° ͜ʖ ͡°)" name="lastname">
        </label>
        <label>Country:
            <input type="text" placeholder="( ͡° ͜ʖ ͡°)" name="country">
        </label>
        <label>City:
            <input type="text" placeholder="( ͡° ͜ʖ ͡°)" name="city">
        </label>
        <button type="submit" name="submit">Create</button>
    </form>

    <?php } else { 
            if($_POST['username'] === '') {
                echo 'Please enter username.';
            } else if($_POST['password'] === '') {
                echo 'Please fill in your password.';
            } else if($_POST['email'] === '') {
                echo 'Please fill in your email.';
            } else {
                echo 'User created.';
            }
         } 
} ?>
