<?php require __DIR__ . '/../config.php';

if(!isset($_GET['user'])) {
    throw new DomainException('ERR: NO USER.');
} else {
    if (!$db->getUser($_GET['user'])) {
        throw new DomainException('ERR: NO USER WITH ID.');
    } else {
        if ($db->deleteUser($_GET['user'])) {
            echo 'User Deleted.';
        }
    }
}
