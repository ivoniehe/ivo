<?php require __DIR__ . '/../config.php';

if(!isset($_GET['user'])) {
    throw new DomainException('No user found.');
} else {
    if(!isset($_POST['submit'])) {
        $user = $db->getUser($_GET['user']); ?>

        <h1>Edit user.</h1>
        <form action="edit.php?user=<?= $user['uniqid']; ?>" method="post">
            <label>Username:
                <input type="text" value="<?= $user['username']; ?>" name="username">
            </label>
            <label>Email:
                <input type="text" value="<?= $user['email']; ?>" name="email">
            </label>
            <label>Password:
                <input type="password" value="<?= $user['password']; ?>" name="password">
            </label>
            <label>Firstname:
                <input type="text" value="<?= $user['firstname']; ?>" name="firstname">
            </label>
            <label>Lastname:
                <input type="text" value="<?= $user['lastname']; ?>" name="lastname">
            </label>
            <label>Country:
                <input type="text" value="<?= $user['country']; ?>" name="country">
            </label>
            <label>City:
                <input type="text" value="<?= $user['city']; ?>" name="city">
            </label>
            <button type="submit" name="submit">Create</button>
        </form>
    <?php } else if(isset($_POST['submit'])) {
        $data = array(
            'username' => $_POST['username'],
            'email' => $_POST['email'],
            'password' => $sec->hashPass($_POST['password']),
            'firstname' => $_POST['firstname'],
            'lastname' => $_POST['lastname'],
            'country' => $_POST['country'],
            'city' => $_POST['city'],
        );

        if(!$db->updateUID('users', $_GET['user'], $data)) {
                echo 'Failed to update user.';
            } else {
                echo 'Succesfully updated user.';
            }
        } 
    }