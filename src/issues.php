<?php require __DIR__ . '/config.php';
$issues = $db->getAll('issues');
foreach ($issues as $issue) {
    $dateMonth = date('F', strtotime($issue['date']));
    $dateDay = date('D', strtotime($issue['date']));
    ?>
    <h4><?= $issue['header'] ?></h4>
        <?= $issue['detail'] ?>
        <br />
        Status: <?php if($issue['status'] === '1') { echo 'Enabled'; } else { echo 'Disabled'; } ?></p>
        <br />
        Date: <?= $dateMonth . ' ' . $dateDay; ?>
        <br />
        <a href="issue/issue.php?issue=<?= $issue['uniqid'] ?>">View issue</a>
        <a href="issue/delete.php?issue=<?= $issue['uniqid'] ?>">Delete Issue</a>
<?php } ?>
