<?php require __DIR__ . '/../config.php';

if(!isset($_GET['issue'])) {
    throw new DomainException('ERR: NO ISSUE.');
} else {
    if (!$db->getIssue($_GET['issue'])) {
        throw new DomainException('ERR: NO ISSUE WITH ID.');
    } else {
        if ($db->deleteIssue($_GET['issue'])) {
            echo 'Issue deleted.';
        }
    }
}
