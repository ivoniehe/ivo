<?php require __DIR__ . '/../config.php';

if(!isset($_GET['issue'])) {
    throw new DomainException('ERR: NO ISSUE.');
} else {
    if(!$db->getIssue($_GET['issue'])) {
        throw new DomainException('ERR: NO ISSUE WITH ID.');
    } else {
        $issue = $db->getIssue($_GET['issue']);
    }
} ?>
<h1>Viewing issue: <?= $issue['header']; ?></h1>
<form action="save.php?issue=<?= $issue['uniqid'] ?>" method="post">
    <label>Title:
        <input type="text" value="<?= $issue['header'] ?>" name="header">
    </label>
    <label>Able to reproduce?:
        <input type="text" value="<?= $issue['reprod'] ?>" name="reprod">
    </label>
    <label>Details:
        <input type="text" value="<?= $issue['detail'] ?>" name="detail">
    </label>
    <button type="submit" name="submit">Save</button>
</form>