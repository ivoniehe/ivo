<?php require __DIR__ . '/../config.php';

if(!isset($_GET['user'])) {
    throw new DomainException('No user found.');
} else {
    if(!isset($_POST['submit'])) { ?>
        <form action="save.php?issue=<?= $_GET['issue'] ?>" method="post">
            <label>Title:
                <input type="text" value="<?= $_POST['header'] ?>" name="header">
            </label>

            <label>Able to reproduce?:
                <input type="text" value="<?= $_POST['reprod'] ?>" name="reprod">
            </label>
            <label>Details:
                <input type="text" value="<?= $_POST['detail'] ?>" name="detail">
            </label>
            <button type="submit"name="submit">Save</button>
        </form>
        <?php
    } else {
        $uniqid = $_GET['issue'];
    $data = [
        'header' => $_POST['header'],
        'detail' => $_POST['detail'],
        'reprod' => $_POST['reprod'],
    ];

    if(!$db->updateUID('issues', $uniqid, $data)) { 
        echo 'Error with updating issue.';
    } else {
        echo 'Issue updated.';
    }
    }
}