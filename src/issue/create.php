<?php require __DIR__ . '/../config.php';

if(!isset($_POST['submit'])) { ?>
    <title>Issue</title>

    <h1>Create issue.</h1>
    <form action="create.php" method="post">
        <label>Title:
            <input type="text" placeholder=".medium-6.columns" name="header">
        </label>
        <label>Able to reproduce?:
            <input type="text" placeholder="Yes / No" name="reprod">
        </label>
        <label>Details:
            <input type="text" placeholder="I have encountered this issue by ..." name="detail">
        </label>
        <button type="submit" class="button float-center" name="submit">Create</button>
    </form>
<?php } else {
    $header = $_POST['header'];
    $uniqID = base_convert(sprintf('%u', crc32(serialize($header))), 10, 36);
    $data = [
        'uniqid' => $uniqID,
        'header' => $header,
        'detail' => $_POST['detail'],
        'reprod' => $_POST['reprod'],
    ];

    if(!$db->insert('issues', $data)) { ?>
        <h1>Issue while creating issue.</h1>

        <form action="create.php" method="post">
            <label>Title:
                <input type="text" placeholder=".medium-6.columns" name="header">
            </label>
            <label>Able to reproduce?:
                <input type="text" placeholder="Yes / No" name="reprod">
            </label>
            <label>Details:
                <input type="text" placeholder="I have encountered this issue by ..." name="detail">
            </label>
            <button type="submit" name="submit">Create</button>
        </form>
    <?php if($_POST['header'] === '') {
                echo 'Please enter header.';
            } else if($_POST['detail'] === '') {
                echo 'Please fill in your details.';
            } else if($_POST['reprod'] === '') {
                echo 'Please fill in your reproduction value.';
            } else {
                echo 'Issue created.';
            }
    }
}